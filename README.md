## 👋 Hi, I’m @RasheedAlwahbany Software Developer

- I am a software developer with three years of practical experience in efficiently programming websites and Android applications using modern programming languages such as Java, PHP and Python as well as coding and design languages such as XML, HTML, CSS and JavaScript. Currently, I specialize in website development using Django web framework and ASP.net mvc and have great knowledge of Reactjs and Nextjs.
We also worked on customizing a copy of the ODOO system
I am committed to proactive feature improvement and bug fixing to ensure we develop modern, easy-to-use websites and applications.
I am passionate about software development and am confident that I will be an excellent developer. I have excellent interpersonal skills and I can work effectively as part of a team or individually to achieve project goals because we use github, gitlab with GIT version control.


- Date and place of birth in Yemen, Taiz, Sharaab Al-Rawanah at 1996/11/6.
- Social links and profiles:<br/><br/>
    <a href="mailto:rasheedalwahbany@gmail.com">Gmail</a> | 
    <a href="mailto:rasheed.alwahbany@outlook.com">Outlook</a> | 
    <a href="https://gitlab.com/RasheedAlwahbany/">GitLab</a> | 
    <a href="https://www.facebook.com/rasheedalwahbany/">Facebook</a> | 
    <a href="https://www.linkedin.com/in/rasheedalwahbany/" >Linked In</a> | 
    <a href="https://people.bayt.com/rasheedalwahbany/" >Bayt Profile</a> | 
    <a href="https://www.for9a.com/user/profile" >For9a Profile</a> | 
    <a href="https://www.freelancer.com/u/RasheedAlwahbany" >Freelancer Profile</a> | 
    <a href="https://profile.indeed.com/?hl=en_US&co=US&from=gnav-homepage" >Indeed Profile</a> | 
    <a href="https://www.upwork.com/freelancers/~0187655cc0a7b86d39" >Upwork Profile</a> | 

- My projects: 
   - <a href="https://www.agbartec.com/">AGBAR company website</a> 

![Rasheed CV](https://github.com/RasheedAlwahbany/RasheedAlwahbany/blob/main/Rasheed.CV.png)

[Rasheed CV .pdf](https://github.com/RasheedAlwahbany/RasheedAlwahbany/blob/main/Rasheed.CV.pdf)

<!---
RasheedAlwahbany/RasheedAlwahbany is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
<!-- <a href="https://www.instagram.com/rasheedalwahbany/">Instagram</a> | --!>
